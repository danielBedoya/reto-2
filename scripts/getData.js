new Vue({
    el: '#app',
    data() {
        return {
            info: null,
            hombres: [],
            mujeres: [],
            estado: {
                asintomatico: [],
                leve: []
            }
        }
    },
    methods: {
        deleteTarea(index) {
            this.estado.asintomatico.splice(index, 1);
        },
    },
    mounted() {
        axios
            .get('https://www.datos.gov.co/resource/gt2j-8ykr.json')
            .then(response => {
                this.info = response.data;
                response.data.map((item) => {
                    if (item.sexo === "F") {
                        this.mujeres.push(item);
                    } else if (item.sexo === "M") {
                        this.hombres.push(item);
                    }
                    if (item.estado === "Asintomático") {
                        this.estado.asintomatico.push(item);
                    } else if (item.estado === "Leve") {
                        this.estado.leve.push(item);
                    }
                })
                    
            })
    }
})